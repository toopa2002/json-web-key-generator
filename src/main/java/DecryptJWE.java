import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jose.crypto.RSADecrypter;
import com.nimbusds.jose.crypto.RSAEncrypter;
import com.nimbusds.jose.crypto.factories.DefaultJWEDecrypterFactory;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jwt.EncryptedJWT;

import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.text.ParseException;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.crypto.KeyGenerator;

public class DecryptJWE {
    public DecryptJWE() throws JOSEException {
    }

    public static void main(String[] args) throws NoSuchAlgorithmException, JOSEException, ParseException {
//        JWEAlgorithm alg = JWEAlgorithm.RSA_OAEP_256;
//        EncryptionMethod enc = EncryptionMethod.A128CBC_HS256;
//
//// Generate an RSA key pair
//        KeyPairGenerator rsaGen = KeyPairGenerator.getInstance("RSA");
//        rsaGen.initialize(2048);
//        KeyPair rsaKeyPair = rsaGen.generateKeyPair();
//        RSAPublicKey rsaPublicKey = (RSAPublicKey)rsaKeyPair.getPublic();
//        RSAPrivateKey rsaPrivateKey = (RSAPrivateKey)rsaKeyPair.getPrivate();
//        System.out.println(rsaPrivateKey);
//// Generate the preset Content Encryption (CEK) key
//        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
//        keyGenerator.init(enc.cekBitLength());
//        SecretKey cek = keyGenerator.generateKey();
//
//// Encrypt the JWE with the RSA public key + specified AES CEK
//        JWEObject jwe = new JWEObject(
//            new JWEHeader(alg, enc),
//            new Payload("Hello, world!"));
//        jwe.encrypt(new RSAEncrypter(rsaPublicKey, cek));
//        String jweString = jwe.serialize();
//
//// Decrypt the JWE with the RSA private key
//        jwe = JWEObject.parse(jweString);
//        jwe.decrypt(new RSADecrypter(rsaPrivateKey));
//        System.out.println("Hello, world!"+ jwe.getPayload().toString());
//
//// Decrypt JWE with CEK directly, with the DirectDecrypter in promiscuous mode
//        jwe = JWEObject.parse(jweString);
//        jwe.decrypt(new DirectDecrypter(cek, true));
//        System.out.println("Hello, world!"+jwe.getPayload().toString());


        String pemEncodedRSAPrivateKey =
            "-----BEGIN PRIVATE KEY-----\n"+
            "MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCKfkiAJjyuIDRG\n"+
            "pfYRbO/Z2IkDaXpi2dZqBpSdMH8QSgxCGTWNM5ATeKZjCDZtAMjLnlBuYBXcBviE\n"+
            "N6puy6nFjun+j5LmMcEe2oU1ulO7Rma2T+bPK4Uy4JML7McX3Xci56OGFCipha0v\n"+
            "a+ePCIfGblT3uPaQhmaoJmnH2n4raBV0JKYNRZPQS36IddcCzMTrJglMQtK1woPq\n"+
            "3q6a6vtNodGy+ubll7e9z2jo9u+yuhNdVpEorfOw/2qx513IWAWd6TXRJJxbUu64\n"+
            "IS5e2BDBSBe0fFuwtX4tbyDIuJYnGZYqrokptBLsqYytbs0Y0setH4MFm3AOZA+4\n"+
            "QGhV/CP9AgMBAAECggEAVkjNZMHt5lJ8H1QA1eR4HtJwsqaBsJqlopMM/k4brAtm\n"+
            "TbwlCB8K8Yamq2aPeiQV9f5hgBLvK5FQPhfsYHEVCde7Y3USAccX2eAtREcdKb13\n"+
            "+rgXRhWIPCV8l95iQDUjp6SH1d4zY0K8nqHst8mJxhAxcdFUFMEXrkWYfAJdvy58\n"+
            "X6EZNjNZjiqXlyiZnCzZvBHVino+udpddMUdlsPB2IV02sMxV6kcPgli9ig0zWog\n"+
            "Jf4UirRPLBKRjE3buvP2KWBdsZ4Sv2M+QK4WEyUupK68Wm0nWIe73GZwjwt70L1e\n"+
            "yCa1p9uqS/f0J0tJzdIoIWcjEsVgH2T0q7U9v4FqIQKBgQDzbSwouDtSoRk+YZWX\n"+
            "AiD5DHEulfc7wrium8ySRb5BY966P/tX5EtdjnARxWwNiIkG9Zu+vMXRUrvn07U7\n"+
            "K/qzXSMFg/MtnIAWTBj70eIIQt9xtzPiYAkD300SH/4zinuJaV23mEqfHBuxRUic\n"+
            "4/XM0UWdemtErJEhYazJEaOo5QKBgQCRpZRpsyIEvAhdK3hzFWovHT/4TAyr5V6q\n"+
            "z8qTrzGM011ZnmWsXgGS2Z+ZWafX+pX7SCKVOU+UXl2btzur2Bxig76KBY5ywVGs\n"+
            "DS5huqDXWJmO90LOtgerM73l+L+iRGHQJ59RKpRuN9Prs0oQLHZE7RKNqUe2iMid\n"+
            "6xN9oaHVOQKBgQDii3fNbafH/qW/qJHPN+v329x5vpeYxl+aIOUCPGYT9R4Tt/fy\n"+
            "lESPjUA/SbyF28uq7mlD2etAI/88gYroxsVKqCMvvt1SVZPfLHMT6QCzL4lEC3v9\n"+
            "8eJx+B+B/sRUs0zFVYoik1jfOwmKsTAi0wSu2RQwuv2ZrABlUMG9Xu0xAQKBgC/U\n"+
            "9Ce+d7acVhbbFuScrNGqRn7x4tt1KaDpOvtDdv9rzuWGupdgDB5BMEP5iMDKgAUV\n"+
            "TJ47QyJT12QzSfqGr9O+to23g2+2g28AqTHZS8dNRqkIFT2z1FLqXDwxdCYDVSg6\n"+
            "Sv0qAbMszIFE0Vb8cx7xyL0TUgTlaRVLxYvl0DMpAoGAUdmZ5HeFcNFDX8mLTDqd\n"+
            "TAIZtJBswHbTKYk8GD0rFAxnYIgQMlU2GaxIpOeDIyt9SMiqUDG+t+uJa9KPt4vg\n"+
            "wEbQcobVK5WBBTSLRMllzvIVvgxi0LbBYj7XPsh9Tz4FyKdLB4IalEPug3PkK4lG\n"+
            "rukVblyI8YGDRscPP4U9XHU=\n"+
            "-----END PRIVATE KEY-----";
        JWK jwk = JWK.parseFromPEMEncodedObjects(pemEncodedRSAPrivateKey);
        System.out.println(jwk);



        String idToken="eyJ0eXAiOiJKV1QiLCJraWQiOiJJb2tnY09CdVRVUjUtOV9jSEtYWlpETzNDN19KMjhGWFkwU2lLOG5rbVJzIiwiY3R5IjoiSldUIiwiZW5jIjoiQTEyOENCQy1IUzI1NiIsImFsZyI6IlJTQS1PQUVQLTI1NiJ9.YuvvOAD7O1AW8bDLmVRZtKcf1_9r4nMl14xC18vC70_ofz4gXJTvrCti_1z-DedhnkQnM9Ef3MK2aN44S7ksx5NhuVCUoVrDnncaCEZYiRqoogYtdeytrtEEIKmRjIYCZ3KYs-SNZitIRET_zzX8CjwOzkkFm2ueV2GRAB8nxRIlQPRYIt-oo63X4sxMT4APJU2E6LV9Q2ZQR8PAAwO9XNdXkHEEIMO1Ue2FoS7IVZoCf71n7n4MthO9ckOxiTdbOHs28GU3L_ZBLOrkzKbmx-FhmBMjWu0xklIYlmKmn55pWEnR5NszRTmUUIrAtu7Q_BoIelhY2uSkNPKi5a2lEw.ivDs3Eiw6fPJeotLxr-A3A.e8riJNgrjhIqviK-Bf9nzVQyioauh-a4M13TzfNkggN4V0KaHo39o0vC4DXFYkXo10oZi5SL88vJkojn_0CeL6y66qhwbYzfENpzP9gyt3iIaFS0Mby8950A7T-QFW95x2yI3Uwv2HimaB1R6-UJxU9xSJ48BHnRyPAdnhM7BdzYn4jAnd_VPjNHGF2awjNaNtx7_Z-PNamaZtX6ZFtvvs_rx8UBMzqRSiHVNue77ugDhUBIF7sB-Ft1-LCIHtsn8opubbZKz0_mqtdiuv0GiaTRhHGGnhX5kx7j7akNigMShvZ3oNHEwAyIAma-jeEfBfYUasf1wumQ6Q5JKHBZkurMqY83WzylNcDRyZq6nTqlBVjUfYyJGRnS3DoeruEU2mWCpETdmiDIKk2fH-05-CYevAGabTgUaPnlNXLsas9DeFWl-9BYoYM3M0vC8NR3zx7ER4Sx8ogzXYEHMLX9EUK6luMiRC7jUzxwZekNz4NgpZJSlV4sqbhvaeDGEkHGzJyysLIQ3wPnteDL_Gdj4SfjmXp2Y4l82_JHnUAMbBeoU--UkGbTG_dX6BSikv-DGkH1WWIA9hiChciOOsBxsqO7WVvsCZYJumaYu_J0aTK1-M8QbO5j3k40HTCRG5l_tgdvvUjFGTLhhTHs0ZOK-PqpBIO7spJcBdZoOFYhZ9nPbKXEPRXUFqTfTQI3_mefpkHSKDwMG8tPV9oMfhIGI8wFx3M4yJHqmOoppMu5Vb0B-0vMluJBb9MdBz9BheCtZCTHQLbCqW-IQ74jy_CkDiEeIPQMFr4C38kVi_81djGJjzwwAtFusqF9zJ-7Okzrf125krgYJt05iqk0sWVfy8Qt0db3RDeAKrqlpsnbFhFUV7uD-iNi7JjFpAjgyOTAckC8VBksV2UT0GLeAG9zNsxdCf8lrz4YbjgA-LEZtYIt8_Ha4PGHmffo-yAd-kVutDLIiwGTNIwQLR2brDoHP5ppJyDpsM7pZrmoQsZAgxVL-iCeBv3hm4SjiUnohi-UlhZsz_YU_9AvufPqnytK_oZiOxivsSQt9wRQ41W9vPiUPUsARjrJCHNKOH65Ru31f6S5zeOOaPixEzRaE63-9jN76xg4te0qSHPXFgslfT-evxjiJw-IfNfBlP46QU8DY0Uzwek5axNdOM2h1IUOr35L7SEt-vPKYszt8jjE2ZrmxlCy1Re67goZWgKdtHC3f891lplDgsoq7Hf9T5E_aiYNuM6CZ6YZV5qeIZCe_zS3dMx-n_UgJLvMNRze14sXU2xiOS-MborUaTrPfpKKmQvH2tNEvXZVE-J-0q1qKgAMXIvCvmU-ujKBDLfc8rOOi-J9LXJdlJeOFI6RSoq3kwObAq6x9Q_sO70swRD-xOhcx_I9BHdCBsKxv34Cf5NKT5J6rrPFoVOXMtc5n1QnyToOA9ibc6jYEWAZRXF5EWfSKaqHdiF3Yee88CjERm_gD9CZl6Gi3mMIp7sUqjOM7pi1IBd6ZjmQFw_hL6CMmSIeZmRvlfte3dplF1XCO_IZaQBeWPMiPJtQl6_LUxzJvmvOZQJ3gOe92Z4o_aewzf5P52Al6L5gKO0TvYWDiT1vtPXwkJRG9i6J9Kj53-OIVbm43wpjeOLWcYUTOZfK2lVgRf_MYQbKfbtR27IHXN1E0jkeOf4B5EShMdAsoImCgOUjydOgf7vPUYUpq0Hwjn3SO97VBF-u_SZl4EuEvpnShRX3w3LIBw_cBtaAem7QfQ.FPyUaRiD2Peift2qKi1zCQ";

//        String idToken="eyJ0eXAiOiJKV1QiLCJraWQiOiJEa0tNUEU3aEZWRW43N1dXaFZ1emFvRnA0Tzg9IiwiY3R5IjoiSldUIiwiZW5jIjoiQTEyOENCQy1IUzI1NiIsImFsZyI6IlJTQS1PQUVQLTI1NiJ9.WVRSnWdGva9gVGIprinEQcqbHApSqr7wCrmH8AW5amojoMXWxtUENdJP9Ery6W8pwo0VGtMKq7rtTJr8QKoaP5xFGzwbddZJiJrlf8FNuXcm-uPxTpwu4CXsArYoku09GjZNCQ-MTkyXBfwjN64cluKY2A4nXudGWFxPWpB4-bMROoL8AR8H-_FQMWswRS-r-jXb9gugEucnY-ELRC6HA_FIvbsSdT8sXnQ1c_Azw23DQS8pKfasvsVZNseF9mHSHR4PVmIP8F8zJm1UjAVatXPh-Gw6_oCIMkbwC9ila1aZfIdA6wSBGtDFdRmRbOpdOlDXiQA3kfVuVr1bXsObqg.ID7qbPDWoHOrt_4W4r7lSg.5BKdABqAHtwLmKj82dgmWcAGD3dBtswAVs6n5_QnZKc7hLREi-ewlge-Ze93qsli1Fiyn014c30HTBM0NH1wZbBIJ1MrErZUIJwKnyEa7VtQbSSk_vnMUeXG8XHJtISOzQTTN6kO6xUyRauTTxlbm1ueVaZYX4GLHr5qzOzfKhVRnPsifeNRqCCsLgsnHeVp17jrXseTHJuonXZ4sot7Ii2R9qkpLs3Ihsi9t3V3gfdWTZ1MyywewL0Ug1o0i18H_LQwQJHxqwoZzO2Goqdw-DrPobcft9iFNXwyY5WP8_3A-dVEYieoLT0di0kdoOy_Z46tDaY0XybSwgm-aC192-tbU3q05-LntJxbH9eJPOpnftZFKHa4-8Y-SblJxurKC4uKHNDw_pL5YmPjC2q_WXQ-zeoq-6yYPydt093DkNyw3nDJ7tHHzxLgdiIT8m_uqn_v6XFWdWuDf5yTtPeEZrm6Ze5hVHRhHMuvVoVYP_vSgBoxIDje3h90NKVE1C87MTs-srklgHxdl5ak5QCupPwB4yrGpcBKFcqBWMI-TSYtRLhkSdab_GIi9NG-k95-BdU3eBsXgi9kHNnsInX3BBbM84zTC9ku-EhooUeodCa2bKJcuG9gSFBwc12Bei63ARrJyOcC05ghqm2TQzbt5ODf9NqvgXhp4a2ZJ6gpELgg-0FdWapv6t_6MdXnCTzOw-eHNDeIOG0JNgn-UI6uL1U6zaPY1dTdL0PhsFyjIUfvp4DNPjQQ084SlRxrNtpO_vNKT5sEMXbhVCu-vA5AgvPNTuMaceM-Hm7EXAXD5jy6qDlAZ6nKvSADszdX79R8Zw7u-SXGHQIlrVmZQncSOAnYy5ZFKHlrISxEZaO3K5Jb24i0HF03WyCOOJiCXUtVYZHx6UqVkj88KcaTQJYQP2icyZkV8dXqMX5eKAyuHsmIRvBCCx25e8iw49R0iY8snn2eIb0-Da5I71HcWBfhfWtj3pKJ6Ek_LCRvSEvVTc7Enr52kJG3_Gw8_vVNnl2-s7pEXe7A0qIl_lDh_-kN3bDRoDmLhmgygffbJZZWFq00dFfAufqyOveGiZVZz_NthEUtYxJWlORJwOYf_9v1u0ayWhMEux8jtqwfP8TS-3WtV_E2BKwHHsrLKS7ASM8GCfEyFKNnArxFHwFgsEtL11H0_srITC9i8B1uXE9xSe4uLRtpnusceVFgX8Ocm98SVfkdP1MUejyIlOpPZmx_-AMYZaijC2Uyl1WmJI696ECoInDjLhxSlX9GAVwAQK9qBOcif5JyIY3yJdApG3GXWo4eVQiwk7uCbYLqzLKTjlUsfe8wGl2pKBvay6-oczJv3zGtac4ymz189NPcfJ5Ar-AAcn5D2kcjirr02IEMq3MfmDrhqmjEwZ0JSkePElOgZCZDHwrQjHmCujwDIXVK_gZLWIpPht-6fOfvfXnmE8dW2dLyuNxWa7k974zGcSmOI9Zz2C1Ytcu8A5MkDa03rT9UPF9dqWogmGl4JD8tqznNZY4ylrcH-yOxPjcesS1KpyUDBWb_a3-R3SzKn7ofh2y7URMqceL1ZW6DLaEUb-wIaVon86pSnO6PLcprS7eWVkR9IPOMVdLLT5u_YqrvllbAH40lfUfX96bZetT2yDATD3g6s22fjhAU-pMc1Ra1.D3Z9FycsTMJmnrqmnAxHog";



        EncryptedJWT encrytedIdTOken=EncryptedJWT.parse(idToken);
        System.out.println(encrytedIdTOken.getHeader().toJSONObject());
        System.out.println(encrytedIdTOken.getCipherText());
        DefaultJWEDecrypterFactory jweDecryptFactory = new DefaultJWEDecrypterFactory();



        encrytedIdTOken.decrypt(jweDecryptFactory.createJWEDecrypter(encrytedIdTOken.getHeader(),jwk.toRSAKey().toPrivateKey()));
        System.out.println("Hello, world!"+ encrytedIdTOken.getPayload());
//
//        JWEObject jweObject=  JWEObject.parse(idToken);
//        jweObject.decrypt(jweDecryptor);
//        System.out.println("Hello, world!"+ jweObject.getPayload().toSignedJWT());
    }


}
